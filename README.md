This script is a wrapper for iostat. It will replace the device mapper with the associated file system.
As is, it grep for LVM volumes. It would need to be modifed to work with non-LVM devices. It's also
written to not show i/o for the local disk. I wrote this for SAN devices. It will only display a single
page of output. I use it with the watch command so that I can change the interval and use -d.

Ex. watch -n 5 ./pyfsstat
